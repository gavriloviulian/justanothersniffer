#include <string>
#include <iostream>
#include "src/TinsSniffer.h"
#include "src/SqliteHandler.h"

using namespace std;
using namespace Tins;
using namespace sniffer;

#define MAX_QUEUE_CAPACITY 1000

int main() {
    string dbfile = "sniffer.db";
    SyncronizedQueue<PacketSniffer> *queue = new SyncronizedQueue<PacketSniffer>(MAX_QUEUE_CAPACITY);
    ISniffer *sniffer = new TinsSniffer(queue);
    SqliteHandler *sqliteHandler = new SqliteHandler(dbfile);
    //sqliteHandler->clearAll();
    sqliteHandler->createTables();
    sniffer->start();
    while (true) {
        PacketSniffer packet;
        queue->consume(packet);
        cout << packet.toString() << endl;
        sqliteHandler->insert(packet);
    }

    delete sniffer;
}
