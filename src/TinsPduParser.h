//
// Created by root on 7/9/18.
//

#ifndef JUSTANOTHERSNIFFER_TINSPDUPARSER_H
#define JUSTANOTHERSNIFFER_TINSPDUPARSER_H

#include "PacketSniffer.h"
#include <string>
#include <sstream>
#include <iostream>
#include<tins/tins.h>
#include <unordered_map>

using namespace std;
using namespace Tins;

namespace sniffer {

    class TinsPduParser {
    public:
        TinsPduParser(PDU *pdu) : _pdu(pdu) {}

        PacketSniffer parse();

        const char *getPDUTypeString(PDU::PDUType pduType);

    private :
        PDU *_pdu;

        int extractHostnameFromTLS(Tins::RawPDU::payload_type payloadType, string &hostname);

        int extractHostameFromTLSExtension(Tins::RawPDU::payload_type data, int pos, string &hostname);

        int extractHostnameFromHttp(Tins::RawPDU::payload_type payloadType, string &hostname);

        const char *parseIpProtocolIANA(uint8_t value);

        string tcpFlagsToString(TCP *tcp);

    };
}


#endif //JUSTANOTHERSNIFFER_TINSPDUPARSER_H
