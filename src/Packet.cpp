//
// Created by root on 7/9/18.
//

#include "PacketSniffer.h"

namespace sniffer {


    const string &PacketSniffer::getA_ip_src() const {
        return a_ip_src;
    }

    void PacketSniffer::setA_ip_src(const string &a_ip_src) {
        PacketSniffer::a_ip_src = a_ip_src;
    }

    const string &PacketSniffer::getB_ip_dest() const {
        return b_ip_dest;
    }

    void PacketSniffer::setB_ip_dest(const string &b_ip_dest) {
        PacketSniffer::b_ip_dest = b_ip_dest;
    }

    const string &PacketSniffer::getC_mac_src() const {
        return c_mac_src;
    }

    void PacketSniffer::setC_mac_src(const string &c_mac_src) {
        PacketSniffer::c_mac_src = c_mac_src;
    }

    const string &PacketSniffer::getD_mac_dest() const {
        return d_mac_dest;
    }

    void PacketSniffer::setD_mac_dest(const string &d_mac_dest) {
        PacketSniffer::d_mac_dest = d_mac_dest;
    }

    uint16_t PacketSniffer::getE_port_src() const {
        return e_port_src;
    }

    void PacketSniffer::setE_port_src(uint16_t e_port_src) {
        PacketSniffer::e_port_src = e_port_src;
    }

    uint16_t PacketSniffer::getF_port_dest() const {
        return f_port_dest;
    }

    void PacketSniffer::setF_port_dest(uint16_t f_port_dest) {
        PacketSniffer::f_port_dest = f_port_dest;
    }

    const string &PacketSniffer::getG_ip_protocol() const {
        return g_ip_protocol;
    }

    void PacketSniffer::setG_ip_protocol(const string &g_ip_protocol) {
        PacketSniffer::g_ip_protocol = g_ip_protocol;
    }

    const string &PacketSniffer::getH_tcp_flags() const {
        return h_tcp_flags;
    }

    void PacketSniffer::setH_tcp_flags(const string &h_tcp_flags) {
        PacketSniffer::h_tcp_flags = h_tcp_flags;
    }

    size_t PacketSniffer::getI_packetSize() const {
        return i_packetSize;
    }

    void PacketSniffer::setI_packetSize(size_t i_packetSize) {
        PacketSniffer::i_packetSize = i_packetSize;
    }

    const string &PacketSniffer::getJ_hostname() const {
        return j_hostname;
    }

    void PacketSniffer::setJ_hostname(const string &j_hostname) {
        PacketSniffer::j_hostname = j_hostname;
    }
}