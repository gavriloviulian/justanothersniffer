//
// Created by root on 7/10/18.
//


#include "HashMapHostnames.h"

namespace sniffer {
    HashMapHostnames *HashMapHostnames::instance = nullptr;
    std::once_flag HashMapHostnames::initInstanceFlag;

    void
    HashMapHostnames::addHostname(string ip_src, uint16_t port_src, string ip_dest, uint16_t port_dest,
                                  string hostname) {
        std::unique_lock<std::mutex> lock(mutex);
        std::ostringstream key;
        key << ip_dest << port_dest;
        if (databaseHostnames.insert(HashmapHostname::value_type(key.str(), hostname)).second == false) {
            databaseHostnames[key.str()] = hostname;
        }
        lock.unlock();

    }

    string HashMapHostnames::getHostname(string ip_src, uint16_t port_src, string ip_dest, uint16_t port_dest) {
        std::unique_lock<std::mutex> lock(mutex);
        std::ostringstream key;
        key << ip_dest << port_dest;
        HashmapHostname::iterator found = databaseHostnames.find(key.str());
        if (found != databaseHostnames.end()) {
            return found->second;
        }
        key.clear();
        key << ip_src << port_src;
        found = databaseHostnames.find(key.str());
        if (found != databaseHostnames.end()) {
            return found->second;
        }
        return "";
    }
}
