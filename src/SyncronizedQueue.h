//
// Created by root on 7/9/18.
//

#ifndef JUSTANOTHERSNIFFER_SYNCRONIZEDQUEUE_H
#define JUSTANOTHERSNIFFER_SYNCRONIZEDQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

namespace sniffer {
    template<typename T>
    class SyncronizedQueue {
        std::condition_variable conditionVariable;
        std::mutex mutex;
        std::queue<T> _queue;
        int maxSize;
    public:
        SyncronizedQueue(int mxsz) : maxSize(mxsz) {}

        void add(T request) {
            std::unique_lock<std::mutex> lock(mutex);
            conditionVariable.wait(lock, [this]() { return !isFull(); });
            _queue.push(request);
            lock.unlock();
            conditionVariable.notify_all();
        }

        void consume(T &request) {
            std::unique_lock<std::mutex> lock(mutex);
            conditionVariable.wait(lock, [this]() { return !isEmpty(); });
            request = _queue.front();
            _queue.pop();
            lock.unlock();
            conditionVariable.notify_all();

        }

        bool isFull() const {
            return _queue.size() >= maxSize;
        }

        bool isEmpty() const {
            return _queue.size() == 0;
        }

        int length() const {
            return _queue.size();
        }

        void clear() {
            std::unique_lock<std::mutex> lock(mutex);
            while (!isEmpty()) {
                _queue.pop();
            }
            lock.unlock();
            conditionVariable.notify_all();
        }
    };
}

#endif //JUSTANOTHERSNIFFER_SYNCRONIZEDQUEUE_H
