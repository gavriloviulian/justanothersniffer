//
// Created by root on 7/9/18.
//

#include "TinsPduParser.h"
#include "HashMapHostnames.h"

#define TLS_HEADER_LEN 5
#define TLS_HANDSHAKE_CONTENT_TYPE 0x16
#define TLS_HANDSHAKE_TYPE_CLIENT_HELLO 0x01

namespace sniffer {

    /*
     * https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers
     *
     */
    const char *TinsPduParser::parseIpProtocolIANA(uint8_t value) {
        switch (value) {
            case 0:
                return "HOPOPT";
            case 1:
                return "ICMP";
            case 2:
                return "IGMP";
            case 3:
                return "GGP";
            case 4:
                return "IP-in-IP";
            case 5:
                return "ST";
            case 6:
                return "TCP";
            case 7:
                return "CBT";
            case 8:
                return "EGP";
            case 9:
                return "IGP";
            case 10:
                return "BBN-RCC-MON";
            case 11:
                return "NVP-II";
            case 12:
                return "PUP";
            case 13:
                return "ARGUS";
            case 14:
                return "EMCON";
            case 15:
                return "XNET";
            case 16:
                return "CHAOS";
            case 17:
                return "UDP";
            case 18:
                return "MUX";
            case 19:
                return "DCN-MEAS";
            case 20:
                return "HMP";
            case 21:
                return "PRM";
            case 22:
                return "XNS-IDP";
            case 23:
                return "TRUNK-1";
            case 24:
                return "TRUNK-2";
            case 25:
                return "LEAF-1";
            case 26:
                return "LEAF-2";
            case 27:
                return "RDP";
            case 28:
                return "IRTP";
            case 29:
                return "ISO-TP4";
            case 30:
                return "NETBLT";
            case 31:
                return "MFE-NSP";
            case 32:
                return "MERIT-INP";
            case 33:
                return "DCCP";
            case 34:
                return "3PC";
            case 35:
                return "IDPR";
            case 36:
                return "XTP";
            case 37:
                return "DDP";
            case 38:
                return "IDPR-CMTP";
            case 39:
                return "TP++";
            case 40:
                return "IL";
            case 41:
                return "IPv6";
            case 42:
                return "SDRP";
            case 43:
                return "IPv6-Route";
            case 44:
                return "IPv6-Frag";
            case 45:
                return "IDRP";
            case 46:
                return "RSVP";
            case 47:
                return "GREs";
            case 48:
                return "DSR";
            case 49:
                return "BNA";
            case 50:
                return "ESP";
            case 51:
                return "AH";
            case 52:
                return "I-NLSP";
            case 53:
                return "SWIPE";
            case 54:
                return "NARP";
            case 55:
                return "MOBILE";
            case 56:
                return "TLSP";
            case 57:
                return "SKIP";
            case 58:
                return "IPv6-ICMP";
            case 59:
                return "IPv6-NoNxt";
            case 60:
                return "IPv6-Opts";
            case 61:
                return "";
            case 62:
                return "CFTP";
            case 63:
                return "";
            case 64:
                return "SAT-EXPAK";
            case 65:
                return "KRYPTOLAN";
            case 66:
                return "RVD";
            case 67:
                return "IPPC";
            case 68:
                return "";
            case 69:
                return "SAT-MON";
            case 70:
                return "VISA";
            case 71:
                return "IPCU";
            case 72:
                return "CPNX";
            case 73:
                return "CPHB";
            case 74:
                return "WSN";
            case 75:
                return "PVP";
            case 76:
                return "BR-SAT-MON";
            case 77:
                return "SUN-ND";
            case 78:
                return "WB-MON";
            case 79:
                return "WB-EXPAK";
            case 80:
                return "ISO-IP";
            case 81:
                return "VMTP";
            case 82:
                return "SECURE-VMTP";
            case 83:
                return "VINES";
            case 84:
                return "IPTM";
            case 85:
                return "NSFNET-IGP";
            case 86:
                return "DGP";
            case 87:
                return "TCF";
            case 88:
                return "EIGRP";
            case 89:
                return "OSPF";
            case 90:
                return "Sprite-RPC";
            case 91:
                return "LARP";
            case 92:
                return "MTP";
            case 93:
                return "AX.25";
            case 94:
                return "OS";
            case 95:
                return "MICP";
            case 96:
                return "SCC-SP";
            case 97:
                return "ETHERIP";
            case 98:
                return "ENCAP";
            case 99:
                return "";
            case 100:
                return "GMTP";
            case 101:
                return "IFMP";
            case 102:
                return "PNNI";
            case 103:
                return "PIM";
            case 104:
                return "ARIS";
            case 105:
                return "SCPS";
            case 106:
                return "QNX";
            case 107:
                return "A/N";
            case 108:
                return "IPComp";
            case 109:
                return "SNP";
            case 110:
                return "Compaq-Peer";
            case 111:
                return "IPX-in-IP";
            case 112:
                return "VRRP";
            case 113:
                return "PGM";
            case 114:
                return "";
            case 115:
                return "L2TP";
            case 116:
                return "DDX";
            case 117:
                return "IATP";
            case 118:
                return "STP";
            case 119:
                return "SRP";
            case 120:
                return "UTI";
            case 121:
                return "SMP";
            case 122:
                return "SM";
            case 123:
                return "PTP";
            case 124:
                return "IS-IS over IPv4";
            case 125:
                return "FIRE";
            case 126:
                return "CRTP";
            case 127:
                return "CRUDP";
            case 128:
                return "SSCOPMCE";
            case 129:
                return "IPLT";
            case 130:
                return "SPS";
            case 131:
                return "PIPE";
            case 132:
                return "SCTP";
            case 133:
                return "FC";
            case 134:
                return "RSVP-E2E-IGNORE";
            case 135:
                return "Mobility Header";
            case 136:
                return "UDPLite";
            case 137:
                return "MPLS-in-IP";
            case 138:
                return "manet";
            case 139:
                return "HIP";
            case 140:
                return "Shim6";
            case 141:
                return "WESP";
            case 142:
                return "ROHC";
            default:
                return "UNASSIGNED";
        }
    }

    const char *TinsPduParser::getPDUTypeString(PDU::PDUType pduType) {
        switch (pduType) {
            case PDU::PDUType::RAW:
                return "RAW";
            case PDU::PDUType::ETHERNET_II:
                return "ETHERNET_II";
            case PDU::PDUType::IEEE802_3:
                return "IEEE802_3";
            case PDU::PDUType::RADIOTAP:
                return "RADIOTAP";
            case PDU::PDUType::DOT11:
                return "DOT11";
            case PDU::PDUType::DOT11_ACK:
                return "DOT11_ACK";
            case PDU::PDUType::DOT11_ASSOC_REQ:
                return "DOT11_ASSOC_REQ";
            case PDU::PDUType::DOT11_ASSOC_RESP:
                return "DOT11_ASSOC_RESP";
            case PDU::PDUType::DOT11_AUTH:
                return "DOT11_AUTH";
            case PDU::PDUType::DOT11_BEACON:
                return "DOT11_BEACON";
            case PDU::PDUType::DOT11_BLOCK_ACK:
                return "DOT11_BLOCK_ACK";
            case PDU::PDUType::DOT11_BLOCK_ACK_REQ:
                return "DOT11_BLOCK_ACK_REQ";
            case PDU::PDUType::DOT11_CF_END:
                return "DOT11_CF_END";
            case PDU::PDUType::DOT11_DATA:
                return "DOT11_DATA";
            case PDU::PDUType::DOT11_CONTROL:
                return "DOT11_CONTROL";
            case PDU::PDUType::DOT11_DEAUTH:
                return "DOT11_DEAUTH";
            case PDU::PDUType::DOT11_DIASSOC:
                return "DOT11_DIASSOC";
            case PDU::PDUType::DOT11_END_CF_ACK:
                return "DOT11_END_CF_ACK";
            case PDU::PDUType::DOT11_MANAGEMENT:
                return "DOT11_MANAGEMENT";
            case PDU::PDUType::DOT11_PROBE_REQ:
                return "DOT11_PROBE_REQ";
            case PDU::PDUType::DOT11_PROBE_RESP:
                return "DOT11_PROBE_RESP";
            case PDU::PDUType::DOT11_PS_POLL:
                return "DOT11_PS_POLL";
            case PDU::PDUType::DOT11_REASSOC_REQ:
                return "DOT11_REASSOC_REQ";
            case PDU::PDUType::DOT11_REASSOC_RESP:
                return "DOT11_REASSOC_RESP";
            case PDU::PDUType::DOT11_RTS:
                return "DOT11_RTS";
            case PDU::PDUType::DOT11_QOS_DATA:
                return "DOT11_QOS_DATA";
            case PDU::PDUType::LLC:
                return "LLC";
            case PDU::PDUType::SNAP:
                return "SNAP";
            case PDU::PDUType::IP:
                return "IP";
            case PDU::PDUType::ARP:
                return "ARP";
            case PDU::PDUType::TCP:
                return "TCP";
            case PDU::PDUType::UDP:
                return "UDP";
            case PDU::PDUType::ICMP:
                return "ICMP";
            case PDU::PDUType::BOOTP:
                return "BOOTP";
            case PDU::PDUType::DHCP:
                return "DHCP";
            case PDU::PDUType::EAPOL:
                return "EAPOL";
            case PDU::PDUType::RC4EAPOL:
                return "RC4EAPOL";
            case PDU::PDUType::RSNEAPOL:
                return "RSNEAPOL";
            case PDU::PDUType::DNS:
                return "DNS";
            case PDU::PDUType::LOOPBACK:
                return "LOOPBACK";
            case PDU::PDUType::IPv6:
                return "IPv6";
            case PDU::PDUType::ICMPv6:
                return "ICMPv6";
            case PDU::PDUType::SLL:
                return "SLL";
            case PDU::PDUType::DHCPv6:
                return "DHCPv6";
            case PDU::PDUType::DOT1Q:
                return "DOT1Q";
            case PDU::PDUType::PPPOE:
                return "PPPOE";
            case PDU::PDUType::STP:
                return "STP";
            case PDU::PDUType::PPI:
                return "PPI";
            case PDU::PDUType::IPSEC_AH:
                return "IPSEC_AH";
            case PDU::PDUType::IPSEC_ESP:
                return "IPSEC_ESP";
            case PDU::PDUType::PKTAP:
                return "PKTAP";
            case PDU::PDUType::MPLS:
                return "MPLS";
            default :
                return "UNKNOWN";
        }
    }


    int TinsPduParser::extractHostameFromTLSExtension(Tins::RawPDU::payload_type data, int pos, string &hostname) {
        size_t len;
        pos += 2;
        if (pos >= data.size())
            return -8;
        while (pos + 3 < data.size()) {
            len = ((size_t) data[pos + 1] << 8) + (size_t) data[pos + 2];
            if (pos + 3 + len > data.size())
                return -1;

            if (data[pos] == 0x00) {
                std::stringstream hostnamess;
                for (int i = pos + 3; i < pos + 3 + len; i++)
                    hostnamess << data[i];
                hostname.assign(hostnamess.str());
                return 0;
            }
            pos += 3 + len;
        }
        return -9;
    }

    /*
     * Based on the information collected from http://blog.fourthbit.com/2014/12/23/traffic-analysis-of-an-ssl-slash-tls-session
     *
     */
    int TinsPduParser::extractHostnameFromTLS(Tins::RawPDU::payload_type payloadType, string &hostname) {
        size_t len;
        size_t pos = 5; // start with the TLS header length
        // check the payload data lenght
        if (payloadType.size() < 5)
            return -1;

        // check for the SNI support in Client Hello
        if (payloadType[0] & 0x80 && payloadType[2] == 1) {
            return -2;
        }

        // check content type to be handshake
        if (payloadType[0] != 0x16) {
            return -3;
        }

        // check the major version of TLS is higher than 2 since only those will support SNI
        if (payloadType[1] < 3) {
            return -4;
        }

        // extensions not supported so no hostname - payload[2] is the minor
        if (payloadType[1] == 3 && payloadType[2] == 0) {
            return -5;
        }

        // check if the handshake type is client hello
        if (payloadType[pos] != 0x01) {
            return -6;
        }

        // skip fixed length records
        pos += 38;

        // skip the session section by reading the length of it and incrementing pos with this */
        len = (size_t) payloadType[pos];
        pos += 1 + len;

        // skip the Cipher Suites section by reading the length of it and incrementing pos with this */
        len = ((size_t) payloadType[pos] << 8) + (size_t) payloadType[pos + 1];
        pos += 2 + len;

        // skip the Compression Methods section by reading the length of it and incrementing pos with this */
        /* Compression Methods */
        len = (size_t) payloadType[pos];
        pos += 1 + len;

        /* Extensions */
        if (pos + 2 > payloadType.size())
            return -7;
        len = ((size_t) payloadType[pos] << 8) + (size_t) payloadType[pos + 1];
        pos += 2;

        if (pos + len > payloadType.size())
            return -1;
        /* Parse each 4 bytes for the extension header */
        while (pos + 4 <= payloadType.size()) {
            /* Extension Length */
            len = ((size_t) payloadType[pos + 2] << 8) +
                  (size_t) payloadType[pos + 3];

            /* Check if it's a server name extension */
            if (payloadType[pos] == 0x00 && payloadType[pos + 1] == 0x00) {
                /* There can be only one extension of each type, so we break
                   our state and move p to beginning of the extension here */
                pos += 4;
                return extractHostameFromTLSExtension(payloadType, pos, hostname);
            }
            pos += 4 + len; /* Advance to the next extension header */
        }
        return -10;
    }

    string TinsPduParser::tcpFlagsToString(TCP *tcp) {
        if (tcp == nullptr)
            return "";
        ostringstream ss;
        if (tcp->get_flag(TCP::FIN))
            ss << "FIN;";
        if (tcp->get_flag(TCP::SYN))
            ss << "SYN;";
        if (tcp->get_flag(TCP::RST))
            ss << "RST;";
        if (tcp->get_flag(TCP::PSH))
            ss << "PSH;";
        if (tcp->get_flag(TCP::ACK))
            ss << "ACK;";
        if (tcp->get_flag(TCP::URG))
            ss << "URG;";
        if (tcp->get_flag(TCP::ECE))
            ss << "ECE;";
        if (tcp->get_flag(TCP::CWR))
            ss << "CWR;";
        return ss.str();
    }

    int TinsPduParser::extractHostnameFromHttp(Tins::RawPDU::payload_type payloadType, string &hostname) {
        std::string str = "";
        char ch;
        int printable = 0;
        if (payloadType.size() < 20)
            return -1;
        for (char ch: payloadType) {
            if (isprint(ch)) {
                printable++;
            }
            str += ch;
        }
        int hostIndex = str.find("Host: ", 0);
        if (hostIndex != std::string::npos) {
            int end = str.find("\r", hostIndex + 6);
            if (end != std::string::npos) {
                hostname.assign(str.substr(hostIndex + 6, end - (hostIndex + 6)));
                return 0;
            }
        }
        return -1;
    }

    PacketSniffer TinsPduParser::parse() {
        PacketSniffer packetSniffer = PacketSniffer();

        if (_pdu == nullptr)
            return packetSniffer;

        if (_pdu->pdu_type() != PDU::PDUType::ETHERNET_II)
            return packetSniffer;

        const EthernetII &eth = _pdu->rfind_pdu<EthernetII>();
        packetSniffer.setC_mac_src(eth.src_addr().to_string());
        packetSniffer.setD_mac_dest(eth.dst_addr().to_string());
        packetSniffer.setI_packetSize(eth.size());

        PDU *innerpdu = eth.inner_pdu();
        if (innerpdu == nullptr) {
            return packetSniffer;
        }

        if (innerpdu->pdu_type() == PDU::PDUType::IP) {
            const IP *ip = reinterpret_cast<IP *>(innerpdu);
            packetSniffer.setA_ip_src(ip->src_addr().to_string());
            packetSniffer.setB_ip_dest(ip->dst_addr().to_string());
            packetSniffer.setG_ip_protocol(parseIpProtocolIANA(ip->protocol()));
        } else if (innerpdu->pdu_type() == PDU::PDUType::IPv6) {
            const IP *ip = reinterpret_cast<IP *>(innerpdu);
            packetSniffer.setA_ip_src(ip->src_addr().to_string());
            packetSniffer.setB_ip_dest(ip->dst_addr().to_string());
            packetSniffer.setG_ip_protocol(parseIpProtocolIANA(ip->protocol()));

        } else {
            packetSniffer.setG_ip_protocol(getPDUTypeString(innerpdu->pdu_type()));
        }

        // check if I have any TCP pdu, and if any found extract the flags
        TCP *tcp = _pdu->find_pdu<TCP>();
        if (tcp != nullptr) {
            packetSniffer.setH_tcp_flags(tcpFlagsToString(tcp));
            packetSniffer.setE_port_src(tcp->sport());
            packetSniffer.setF_port_dest(tcp->dport());
            Tins::RawPDU *rawPDU = tcp->find_pdu<RawPDU>();
            string hostNameFound;
            if (rawPDU != nullptr) {
                Tins::RawPDU::payload_type payload = rawPDU->payload();
                // check for SNI in TLS package
                if (extractHostnameFromTLS(payload, hostNameFound) == 0) {
                    packetSniffer.setJ_hostname(hostNameFound);
                    HashMapHostnames::getInstance().addHostname(packetSniffer.getA_ip_src(),
                                                                packetSniffer.getE_port_src(),
                                                                packetSniffer.getB_ip_dest(),
                                                                packetSniffer.getF_port_dest(),
                                                                hostNameFound);
                } else if (extractHostnameFromHttp(payload, hostNameFound) == 0) {
                    packetSniffer.setJ_hostname(hostNameFound);
                    HashMapHostnames::getInstance().addHostname(packetSniffer.getA_ip_src(),
                                                                packetSniffer.getE_port_src(),
                                                                packetSniffer.getB_ip_dest(),
                                                                packetSniffer.getF_port_dest(),
                                                                hostNameFound);
                }
            }
            if (hostNameFound.empty()) {
                packetSniffer.setJ_hostname(
                        HashMapHostnames::getInstance().getHostname(packetSniffer.getA_ip_src(),
                                                                    packetSniffer.getE_port_src(),
                                                                    packetSniffer.getB_ip_dest(),
                                                                    packetSniffer.getF_port_dest()));
            }
        }

        UDP *udp = _pdu->find_pdu<UDP>();
        if (udp != nullptr) {
            packetSniffer.setE_port_src(udp->sport());
            packetSniffer.setF_port_dest(udp->dport());
            return packetSniffer;
        }

        return packetSniffer;
    }

}