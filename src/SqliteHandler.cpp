//
// Created by root on 7/10/18.
//

#include <iostream>
#include "SqliteHandler.h"


namespace sniffer {
    void SqliteHandler::deleteTable(string name) {
        *db << "drop table if exists " + name;
    }

    void SqliteHandler::clearAll() {
        deleteTable("sniffer");
    }

    void SqliteHandler::createTables() {
        *db << "create table if not exists sniffer ("
               " _id integer primary key autoincrement not null,"
               "a_ip_src char(40),"
               "b_ip_dest char(40),"
               "c_mac_src char(20),"
               "d_mac_dest char(20),"
               "e_port_src int,"
               "f_port_dest int,"
               "g_ip_protocol char(20),"
               "h_tcp_flags char(20),"
               "i_packetSize int,"
               "j_hostname char(255)"
               ");";
    }

    bool SqliteHandler::insert(const PacketSniffer packet) {

        uint16_t retries = 100;
        while (retries > 0) {
            try {
                *db
                        << "insert into sniffer(a_ip_src, b_ip_dest, c_mac_src, d_mac_dest, e_port_src, f_port_dest, g_ip_protocol, h_tcp_flags, i_packetSize,j_hostname) values(?,?,?,?,?,?,?,?,?,?)"
                        << packet.getA_ip_src()
                        << packet.getB_ip_dest()
                        << packet.getC_mac_src()
                        << packet.getD_mac_dest()
                        << (int) packet.getE_port_src()
                        << (int) packet.getF_port_dest()
                        << packet.getG_ip_protocol()
                        << packet.getH_tcp_flags()
                        << (int) packet.getI_packetSize()
                        << packet.getJ_hostname();
            } catch (const sqlite::exceptions::locked &ex) {
                --retries;
            } catch (const sqlite::exceptions::busy &ex) {
                --retries;
            } catch (const sqlite_exception &ex) {
                std::cerr << "Error inserting in database:: " << ex.what() << endl;
                return false;
            }
            return true;
            //return db->last_insert_rowid();
        }
        return false;
    }
}