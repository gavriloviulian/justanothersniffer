//
// Created by root on 7/9/18.
//

#ifndef JUSTANOTHERSNIFFER_PACKET_H
#define JUSTANOTHERSNIFFER_PACKET_H

#include <sstream>
#include <iomanip>
#include "string"

using namespace std;

namespace sniffer {
    /*
    a.
    Adresa IP sursa
    b.
    Adresa IP destinatie
    c.
    Adresa MAC sursa
    d.
    Adresa MAC destinatie
    e.
    Port sursa
    f.
    Port destinatie
    g.
    IP Protocol (TCP, UDP, ICMP, IGMP, etc..)
    h.
    Pentru pachetele TCP si informatii despre TCP Flags (SYN, ACK, FIN
    etc..)
    i.
    Marimea datelor din pachet
    j.
    In cazul pachetelor (HTTP/HTTPS) hostname-ul catre care se
    comunica. Acesta informatie poate fi obtinuta pe baza pachetelor mai vechi.
    Informatia colectata poate fi salvata intr-un fisier in orice format dorit (CSV, XML,
    JSON) sau intr-o baza de date. Poti sa alegi modalitatea de implementare pentru
    captura de traffic: libpcap, raw sockets, etc.
     */
    class PacketSniffer {
    public:
        bool operator==(const PacketSniffer &rhs) const;

        bool operator!=(const PacketSniffer &rhs) const;

        PacketSniffer(const string &c_mac_src, const string &d_mac_dest, const string &g_ip_protocol);

        PacketSniffer() : e_port_src(0), f_port_dest(0), i_packetSize(0) {}

        std::string toString() {
            ostringstream ss;
            ss << "ip_src " << setw(15) << a_ip_src << " ";
            ss << "ip_dst " << setw(15) << b_ip_dest << " ";
            ss << "mac_src " << setw(17) << c_mac_src << " ";
            ss << "mac_dst " << setw(17) << d_mac_dest << " ";
            ss << "port_src " << setw(5) << e_port_src << " ";
            ss << "port_dst " << setw(5) << f_port_dest << " ";
            ss << "ip_prot " << setw(6) << g_ip_protocol << " ";
            ss << "tcp_flags " << setw(10) << h_tcp_flags << " ";
            ss << "pcktSize " << setw(6) << i_packetSize << " ";
            ss << "host " << j_hostname << " ";
            return ss.str();
        }

    public:
        const string &getA_ip_src() const;

        void setA_ip_src(const string &a_ip_src);

        const string &getB_ip_dest() const;

        void setB_ip_dest(const string &b_ip_dest);

        const string &getC_mac_src() const;

        void setC_mac_src(const string &c_mac_src);

        const string &getD_mac_dest() const;

        void setD_mac_dest(const string &d_mac_dest);

        uint16_t getE_port_src() const;

        void setE_port_src(uint16_t e_port_src);

        uint16_t getF_port_dest() const;

        void setF_port_dest(uint16_t f_port_dest);

        const string &getG_ip_protocol() const;

        void setG_ip_protocol(const string &g_ip_protocol);

        const string &getH_tcp_flags() const;

        void setH_tcp_flags(const string &h_tcp_flags);

        size_t getI_packetSize() const;

        void setI_packetSize(size_t i_packetSize);

        const string &getJ_hostname() const;

        void setJ_hostname(const string &j_hostname);

    private:
        string a_ip_src;
        string b_ip_dest;
        string c_mac_src;
        string d_mac_dest;
        uint16_t e_port_src;
        uint16_t f_port_dest;
        string g_ip_protocol;
        string h_tcp_flags;
        size_t i_packetSize;
        string j_hostname;
    };

}


#endif //JUSTANOTHERSNIFFER_PACKET_H
