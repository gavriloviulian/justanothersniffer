//
// Created by root on 7/9/18.
//

#ifndef JUSTANOTHERSNIFFER_TINSSNIFFER_H
#define JUSTANOTHERSNIFFER_TINSSNIFFER_H


#include <tins/tins.h>
#include <thread>
#include "ISniffer.h"
#include "PacketSniffer.h"
#include "SyncronizedQueue.h"

using namespace Tins;

namespace sniffer {

    class TinsSniffer : public ISniffer {
        // interface methods for the sniffer
    public:
        void start();

        void stop();

        const std::string &name(void) const;

    public:
        TinsSniffer(const NetworkInterface &iface, SyncronizedQueue<PacketSniffer> *queue);

        TinsSniffer(SyncronizedQueue<PacketSniffer> *queue);

        virtual ~TinsSniffer(void) {}

    protected:
        Sniffer _sniffer;
        NetworkInterface _iface;
        const std::string _name;
        SyncronizedQueue<PacketSniffer> *queue;

    private:
        void doSniffing(void);

    protected:
        bool handler(PDU &pdu);

    };
}


#endif //JUSTANOTHERSNIFFER_TINSSNIFFER_H
