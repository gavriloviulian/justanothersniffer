//
// Created by root on 7/10/18.
//

#ifndef JUSTANOTHERSNIFFER_DATABASEHOSTNAMES_H
#define JUSTANOTHERSNIFFER_DATABASEHOSTNAMES_H

#include <mutex>
#include <string>
#include <unordered_map>
#include <sstream>
#include <algorithm>
#include <iostream>

using namespace std;

namespace sniffer {

    typedef unordered_map<string, string> HashmapHostname;

    class HashMapHostnames {
    public:
        static HashMapHostnames &getInstance() {
            std::call_once(initInstanceFlag, &HashMapHostnames::initSingleton);
            return *instance;
        }

        void addHostname(string ip_src, uint16_t port_src, string ip_dest, uint16_t port_dest, string hostname);

        string getHostname(string ip_src, uint16_t port_src, string ip_dest, uint16_t port_dest);

    private:
        HashmapHostname databaseHostnames;
        std::mutex mutex;

        HashMapHostnames() = default;

        ~HashMapHostnames() = default;

        HashMapHostnames(const HashMapHostnames &) = delete;

        HashMapHostnames &operator=(const HashMapHostnames &)= delete;

        static HashMapHostnames *instance;
        static std::once_flag initInstanceFlag;

        static void initSingleton() {
            instance = new HashMapHostnames;
        }
    };
}


#endif //JUSTANOTHERSNIFFER_DATABASEHOSTNAMES_H
