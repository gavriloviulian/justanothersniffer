//
// Created by root on 7/9/18.
//

#include "TinsSniffer.h"
#include "TinsPduParser.h"


namespace sniffer {
    TinsSniffer::TinsSniffer(const NetworkInterface &iface, SyncronizedQueue<PacketSniffer> *squeue)
            : _sniffer(iface.name()), _iface(iface), _name("TinsSniffer"), queue(squeue) {
    }

    TinsSniffer::TinsSniffer(SyncronizedQueue<PacketSniffer> *squeue)
            : _name("TinsSniffer"), _sniffer(NetworkInterface::default_interface().name()), queue(squeue) {

    }

    void TinsSniffer::doSniffing() {
        _sniffer.sniff_loop(make_sniffer_handler(this, &TinsSniffer::handler));
    }

    void TinsSniffer::start(void) {
        std::thread t(&TinsSniffer::doSniffing, this);

        t.detach();
    }

    void TinsSniffer::stop(void) {
        _sniffer.stop_sniff();
    }

    const std::string &TinsSniffer::name(void) const { return _name; }

    bool TinsSniffer::handler(PDU &pdu) {
        PacketSniffer packet = TinsPduParser(&pdu).parse();
        //cout << sniffer.toString() << endl;
        if (queue != nullptr) {
            queue->add(packet);
        }
        return true;
    }
}