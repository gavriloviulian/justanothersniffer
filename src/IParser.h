//
// Created by root on 7/9/18.
//

#ifndef JUSTANOTHERSNIFFER_IPARSER_H
#define JUSTANOTHERSNIFFER_IPARSER_H

namespace sniffer {
    template<class Result, class Data>
    class IParser {
    public:
        virtual Result parse(Data data) = 0;
    };
}
#endif //JUSTANOTHERSNIFFER_IPARSER_H
