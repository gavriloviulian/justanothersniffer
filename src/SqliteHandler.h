//
// Created by root on 7/10/18.
//

#ifndef JUSTANOTHERSNIFFER_SQLITEHANDLER_H
#define JUSTANOTHERSNIFFER_SQLITEHANDLER_H

#include <string>
#include <sqlite_modern_cpp.h>
#include "PacketSniffer.h"

using namespace std;
using namespace sqlite;

namespace sniffer {
    class SqliteHandler {
    public:
        SqliteHandler(string dbFile) : db(make_shared<database>(dbFile)) {}

        SqliteHandler() = delete;

        void clearAll();

        void createTables();

        bool insert(const PacketSniffer packet);

    private:
        shared_ptr<database> db;

        void deleteTable(string name);
    };
}


#endif //JUSTANOTHERSNIFFER_SQLITEHANDLER_H
