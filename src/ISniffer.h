//
// Created by root on 7/9/18.
//

#ifndef JUSTANOTHERSNIFFER_ISNIFFER_H
#define JUSTANOTHERSNIFFER_ISNIFFER_H

#include <string>

namespace sniffer {
    class ISniffer {
    public:
        virtual void start() = 0;

        virtual void stop() = 0;

        virtual const std::string &name() const = 0;
    };
}


#endif //JUSTANOTHERSNIFFER_ISNIFFER_H
